package com.github.sirius902.snakegame

import com.github.sirius902.snakegame.game._
import scalafx.animation.AnimationTimer
import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.geometry.Insets
import scalafx.scene.Scene
import scalafx.scene.canvas.{Canvas, GraphicsContext}
import scalafx.scene.control.Label
import scalafx.scene.input.KeyCode
import scalafx.scene.layout.{BorderPane, HBox}
import scalafx.scene.paint.Color
import scalafx.scene.text.Font

object ScalaFXMain extends JFXApp {
  val gameWidth = 600.0
  val gameHeight = 600.0

  val canvas = new Canvas(gameWidth, gameHeight)
  canvas.setFocusTraversable(true) // To be able to have key events register

  val gc: GraphicsContext = canvas.graphicsContext2D
  val backgroundColor: Color = Color.DarkGray

  def tileSize(board: Board): (Double, Double) = {
    val Board(cols, rows) = board
    val tileWidth = gameWidth / cols
    val tileHeight = gameHeight / rows // + 1 to fix weird canvas size
    (tileWidth, tileHeight)
  }

  def boardPosToCanvas(board: Board)(position: Vector2[Int]): Vector2[Double] = {
    val (tileWidth, tileHeight) = tileSize(board)
    Vector2[Double](position.x * tileWidth, position.y * tileHeight)
  }

  def drawTile(position: Vector2[Int], board: Board, color: Color): Unit = {
    val (tileWidth, tileHeight) = tileSize(board)
    val canvasPos = boardPosToCanvas(board)(position)
    gc.setFill(color)
    gc.fillRect(canvasPos.x, canvasPos.y, tileWidth, tileHeight)
  }

  def drawSnake(snake: Snake, state: GameState): Unit = {
    snake.pieces.tail.foreach { pos =>
      drawTile(pos, state.board, Color.Green)
    }

    val headColor =
      if (!state.gameOver)
        Color.LightGreen
      else
        Color.FireBrick

    drawTile(snake.pieces.head, state.board, headColor)
  }

  def drawFruit(position: Vector2[Int], state: GameState): Unit = {
    drawTile(position, state.board, Color.Red)
  }

  def renderState(state: GameState): Unit = {
    gc.clearRect(0, 0, gameWidth, gameHeight)
    drawSnake(state.snake, state)
    drawFruit(state.fruit, state)
  }

  val initialState: GameState = GameState.create()

  var state: GameState = initialState

  var lastKeyPressed: Option[KeyCode] = None

  canvas.onKeyPressed = e => lastKeyPressed = Some(KeyCode(e.getCode))

  val keyToDirection: Map[KeyCode, Direction] = Map(
    KeyCode.W -> Direction.Up,
    KeyCode.S -> Direction.Down,
    KeyCode.A -> Direction.Left,
    KeyCode.D -> Direction.Right
  )

  val scoreLabel = Label("Score: 0, High-score: 0")
  scoreLabel.font = Font("Helvetica", 35)
  scoreLabel.padding = Insets(5)

  val gameTimer = AnimationTimer { _ =>
    val inputDirection = lastKeyPressed.flatMap(keyToDirection.get)

    state = SnakeGame.step(inputDirection).runS(state).value

    scoreLabel.text = s"Score: ${state.score}, High-score: ${state.highScore}"
    renderState(state)
  }

  gameTimer.start()

  AnimationTimer { _ =>
    if (state.gameOver) {
      if (lastKeyPressed.contains(KeyCode.Enter)) {
        state = initialState.copy(highScore = state.highScore)
        gameTimer.start()
      } else {
        gameTimer.stop()
      }
    }
  }.start()

  stage = new PrimaryStage {
    title = "Snake Game"
    width = gameWidth + 30
    height = gameHeight + 120
    scene = new Scene {
      fill = backgroundColor
      content = new BorderPane {
        top = scoreLabel
        center = new HBox {
          children = canvas
          margin = Insets(5)
          style =
            """
              |-fx-border-color: black;
              |-fx-border-width: 5;
            """.stripMargin
        }
      }
    }
    resizable = false
  }
}
