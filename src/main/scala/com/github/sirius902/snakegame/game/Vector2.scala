package com.github.sirius902.snakegame.game

case class Vector2[A](x: A, y: A)(implicit n: Numeric[A]) {
  def -(other: Vector2[A]): Vector2[A] =
    copy(x = n.minus(x, other.x), y = n.minus(y, other.y))

  def +(other: Vector2[A]): Vector2[A] =
    copy(x = n.plus(x, other.x), y = n.plus(y, other.y))
}

object Vector2 {
  def zero[A](implicit n: Numeric[A]): Vector2[A] = Vector2[A](n.zero, n.zero)
}
