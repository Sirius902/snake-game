package com.github.sirius902.snakegame.game

sealed trait Direction {
  val vector: Vector2[Int]
  val opposite: Direction
}

object Direction {

  case object Up extends Direction {
    override val vector: Vector2[Int] = Vector2(0, -1)
    override val opposite: Direction = Down
  }

  case object Down extends Direction {
    override val vector: Vector2[Int] = Vector2(0, 1)
    override val opposite: Direction = Up
  }

  case object Left extends Direction {
    override val vector: Vector2[Int] = Vector2(-1, 0)
    override val opposite: Direction = Right
  }

  case object Right extends Direction {
    override val vector: Vector2[Int] = Vector2(1, 0)
    override val opposite: Direction = Left
  }

}
