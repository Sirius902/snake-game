package com.github.sirius902.snakegame.game

import com.github.sirius902.snakegame.game.Direction._

case class Board(columns: Int, rows: Int) {
  def sidePosition(direction: Direction): Int = direction match {
    case Up => top
    case Left => left
    case Right => right
    case Down => bottom
  }

  val left: Int = 0
  val right: Int = columns - 1
  val top: Int = 0
  val bottom: Int = rows - 1

  lazy val tiles: List[Vector2[Int]] = (for {
    col <- 0 until columns
    row <- 0 until rows
  } yield Vector2[Int](col, row)).toList
}
