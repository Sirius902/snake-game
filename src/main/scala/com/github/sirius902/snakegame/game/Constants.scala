package com.github.sirius902.snakegame.game

object Constants {
  val boardWidth = 13
  val boardHeight = 13

  val initialTailSize = 3

  val dt = 0.01
  val updateTime = 0.075
}
