package com.github.sirius902.snakegame.game

import cats.data.NonEmptyList
import monocle.Lens
import monocle.macros.GenLens

import com.github.sirius902.snakegame.game.Direction._

case class Snake(pieces: NonEmptyList[Vector2[Int]], direction: Option[Direction], newDirection: Option[Direction])

object Snake {
  def create(startingPosition: Vector2[Int], tailSize: Int): Snake = Snake(
    NonEmptyList(
      startingPosition,
      List.fill(tailSize)(startingPosition)
    ),
    None,
    None
  )

  def grow(snake: Snake): Snake = {
    val reversedPieces = snake.pieces.reverse
    val newPieces = (reversedPieces.head :: reversedPieces).reverse
    piecesLens.set(newPieces)(snake)
  }

  def grow(snake: Snake, n: Int): Snake = {
    def loop(snake: Snake, counter: Int): Snake = counter match {
      case 0 => snake
      case _ => loop(grow(snake), counter - 1)
    }

    loop(snake, n)
  }

  def moveHead(snake: Snake): Snake =
    snake.direction.map(dir => headLens.set(snake.pieces.head + dir.vector)(snake)).getOrElse(snake)

  def moveTail(snake: Snake): Snake =
    if (snake.direction.nonEmpty)
      tailLens.set(snake.pieces.toList.dropRight(1))(snake)
    else
      snake

  val move: Snake => Snake = moveTail _ andThen moveHead

  def wallCollision(snake: Snake, board: Board): Boolean = {
    val x = headXLens.get(snake)
    val y = headYLens.get(snake)
    val direction = snake.direction

    direction.exists {
      case dir@(Up | Down) if y == board.sidePosition(dir) => true
      case dir@(Left | Right) if x == board.sidePosition(dir) => true
      case _ => false
    }
  }

  val shouldWrap: (Snake, Board) => Boolean = wallCollision

  def wrap(snake: Snake, board: Board): Snake = {
    val x = headXLens.get(snake)
    val y = headYLens.get(snake)
    val direction = snake.direction

    direction.map {
      case dir@(Up | Down) if y == board.sidePosition(dir) =>
        (moveTail _ andThen headYLens.set(board.sidePosition(dir.opposite))) (snake)
      case dir@(Left | Right) if x == board.sidePosition(dir) =>
        (moveTail _ andThen headXLens.set(board.sidePosition(dir.opposite))) (snake)
      case _ => snake
    }.getOrElse(snake)
  }

  def collision(snake: Snake): Boolean =
    if (snake.direction.nonEmpty)
      snake.pieces.tail.contains(snake.pieces.head)
    else
      false

  private val vec2IXLens = GenLens[Vector2[Int]](_.x)
  private val vec2IYLens = GenLens[Vector2[Int]](_.y)

  val headLens: Lens[Snake, Vector2[Int]] = GenLens[Snake](_.pieces.head)
  val headXLens: Lens[Snake, Int] = headLens composeLens vec2IXLens
  val headYLens: Lens[Snake, Int] = headLens composeLens vec2IYLens

  val piecesLens: Lens[Snake, NonEmptyList[Vector2[Int]]] = GenLens[Snake](_.pieces)
  val tailLens: Lens[Snake, List[Vector2[Int]]] = GenLens[Snake](_.pieces.tail)
}
