package com.github.sirius902.snakegame.game

import cats.data.State

import scala.util.Random

object SnakeGame {
  type StateG[A] = State[GameState, A]

  val moveSnake: StateG[Unit] = State.modify { s =>
    if (Snake.shouldWrap(s.snake, s.board))
      s.copy(snake = Snake.wrap(s.snake, s.board))
    else
      s.copy(snake = Snake.move(s.snake))
  }

  val growSnake: StateG[Unit] = State.modify { s =>
    val newSnake = Snake.grow(s.snake)
    s.copy(snake = newSnake)
  }

  val updateSnakeDirection: StateG[Unit] = State.modify { s =>
    val snake = s.snake

    if (snake.newDirection.exists(snake.direction.contains))
      s
    else
      s.copy(snake = snake.copy(direction = snake.newDirection))
  }

  val checkSnakeCollision: StateG[Unit] = State.modify { s =>
    if (Snake.collision(s.snake))
      s.copy(gameOver = true)
    else
      s
  }

  def handleInput(input: Option[Direction]): StateG[Unit] = State.modify { s =>
    if (input.isDefined && !input.exists(s.snake.direction.map(_.opposite).contains))
      s.copy(snake = s.snake.copy(newDirection = input))
    else
      s
  }

  val repositionFruit: StateG[Unit] = State.modify[GameState] { s =>
    val tiles = s.board.tiles.filterNot(s.snake.pieces.toList.contains)
    val newFruit = Random.shuffle(tiles).headOption

    s.copy(fruit = newFruit.getOrElse(Vector2[Int](-1, -1)))
  }

  val handleFruitCollision: StateG[Unit] = for {
    fruit <- State.get[GameState].map(_.fruit)
    snake <- State.get[GameState].map(_.snake)
    collision = snake.pieces.head == fruit
    _ <- if (collision) repositionFruit else State.get[GameState]
    _ <- if (collision) growSnake else State.get[GameState]
  } yield ()

  val updateScore: StateG[Unit] = State.modify { s =>
    s.copy(score = s.snake.pieces.tail.length - s.initialTailSize)
  }

  val updateHighScore: StateG[Unit] = State.modify { s =>
    if (s.score > s.highScore)
      s.copy(highScore = s.score)
    else
      s
  }

  val handleUpdateTime: StateG[Unit] = State.modify { s =>
    if (s.updateAccumulator <= 0)
      s.copy(updateAccumulator = s.updateTime)
    else
      s.copy(updateAccumulator = s.updateAccumulator - s.dt)
  }

  val updateGame: StateG[Unit] = for {
    _ <- updateSnakeDirection
    _ <- moveSnake
    _ <- handleFruitCollision
    _ <- checkSnakeCollision
  } yield ()

  def step(input: Option[Direction]): StateG[Unit] = for {
    _ <- handleInput(input)
    acc <- State.get[GameState].map(_.updateAccumulator)
    _ <- if (acc <= 0) updateGame else State.get[GameState]
    _ <- updateScore
    _ <- updateHighScore
    _ <- handleUpdateTime
  } yield ()
}
