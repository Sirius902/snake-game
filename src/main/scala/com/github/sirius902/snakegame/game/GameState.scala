package com.github.sirius902.snakegame.game

case class GameState(
                      board: Board,
                      snake: Snake,
                      initialTailSize: Int,
                      fruit: Vector2[Int],
                      input: Option[Direction],
                      score: Int,
                      highScore: Int,
                      gameOver: Boolean,
                      dt: Double,
                      updateTime: Double,
                      updateAccumulator: Double
                    )

object GameState {
  def create(): GameState = GameState(
    Board(Constants.boardWidth, Constants.boardHeight),
    Snake.create(Vector2(4, Constants.boardHeight / 2), tailSize = Constants.initialTailSize),
    initialTailSize = Constants.initialTailSize,
    fruit = Vector2(Constants.boardWidth - 3, Constants.boardHeight / 2),
    None,
    score = 0,
    highScore = 0,
    gameOver = false,
    dt = Constants.dt,
    updateTime = Constants.updateTime,
    updateAccumulator = 0.0
  )
}
